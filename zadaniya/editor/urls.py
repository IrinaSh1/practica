from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^personal$', views.PersonalListView.as_view(), name='personal'),
    url(r'^add-news$', views.NewsCreateView.as_view(), name='add_news'),
    url(r'^add-news/(?P<pk>.+)$',
        views.NewsUpdateView.as_view(), name='add_news'),
]
