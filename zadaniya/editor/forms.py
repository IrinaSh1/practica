from django import forms

from zadaniya.models import News

from datetime import datetime


class NewsModelForm(forms.ModelForm):
    def __init__(self, editor=None, user=None, *args, **kwargs):
        if user is not None and editor is not None:
            self.editor = editor
            self.user = user
        super(NewsModelForm, self).__init__(*args, **kwargs)

    class Meta:
        model = News
        fields = ('name', 'text')

    def save(self):
        news = super(NewsModelForm, self).save(commit=False)
        news.slug = self.cleaned_data.get('name').replace(' ', '-')
        news.created = datetime.now()
        news.changed = datetime.now()
        news.user = self.user
        news.editor = self.editor
        news.save()
        return news
