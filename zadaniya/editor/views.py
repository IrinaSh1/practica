# -*- coding: utf-8 -*-
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView, CreateView, UpdateView

from zadaniya.models import News
from editor.models import Editor

from .forms import NewsModelForm


def is_user_editor(user):
    try:
        Editor.objects.get(user=user)
    except Editor.DoesNotExist:
        return False
    return True

class PersonalListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    template_name = u'editor/personal.html'
    context_object_name = 'news_list'

    def get_queryset(self):
        editor = Editor.objects.get(user=self.request.user)
        news_list = News.objects.filter(editor=editor)
        paginator = Paginator(news_list, 5)
        page = self.request.GET.get('page')
        try:
            news = paginator.page(page)
        except PageNotAnInteger:
            news = paginator.page(1)
        except EmptyPage:
            news = paginator.page(paginator.num_pages)
        return news

    def get_context_data(self, **kwargs):
        context = super(PersonalListView, self).get_context_data(**kwargs)
        context.update({
            'next_for_logout': True,
        })
        return context


class NewsCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    login_url = '/login'
    raise_exception = True
    model = News
    form_class = NewsModelForm
    template_name = u'editor/add_news.html'
    success_url = reverse_lazy('edit:personal')

    def test_func(self):
        return is_user_editor(self.request.user)

    def get_form_kwargs(self):
        kwargs = super(NewsCreateView, self).get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
            'editor': self.request.user.editor,
        })
        return kwargs


class NewsUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    login_url = '/login'
    raise_exception = True
    model = News
    form_class = NewsModelForm
    template_name = u'editor/add_news.html'
    success_url = reverse_lazy('edit:personal')

    def test_func(self):
        return is_user_editor(self.request.user)

    def get_form_kwargs(self):
        kwargs = super(NewsUpdateView, self).get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
            'editor': self.request.user.editor,
        })
        return kwargs
