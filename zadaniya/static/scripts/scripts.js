(function(){
    function get_error_el(el_name, text) {
        var error = document.createElement('ul');
        error.className = 'errorlist ' + el_name + '-error';
        error.innerHTML = '<li>' + text + '</li>';
        return error;
    }

    function insert_error_el(form, el, text) {
        if (!document.getElementsByClassName(el.name + '-error')[0]) {
            var error = get_error_el(el.name, text);
            var sibling = el.parentElement.nextElementSibling;
            form.insertBefore(error, sibling);
        }
    }

    function remove_error(el) {
        var error_el = document.getElementsByClassName(el.name + '-error')[0];
        if (typeof error_el != 'undefined') {
            error_el.remove();
        }
    }

    function is_email_valid(email, form) {
        var is_valid = true;
        if (email.value.length != 0) {
            if (!email.value.match(/^[\w-]+@[\w-]+\.[\w-]+$/)) {
                remove_error(email);
                insert_error_el(form, email, 'Email must be like "test@test.com"');
                is_valid = false;
            } else {
                remove_error(email);
            }
        } else {
            if (this.className == 'news_item-form') {
                insert_error_el(form, email, 'Email is required!');
                is_valid = false;
            }
        }
        return is_valid;
    }

    function is_password_valid(password, form) {
        if (typeof password != 'undefined') {
            if (!/(?=.*\d+)(?=.*[a-z]+)(?=.*[A-Z])/.test(password.value) ||
                    password.value.length <= 5) {
                insert_error_el(form, password,
                    'Password must contain digit, latin symbols (upper and lower), and it must contain at least 6 symbols');
                return false;
            } else {
                remove_error(password);
            }
        }
        return true;
    }

    function validate_phone(phone) {
        phone.value = phone.value.replace(/\D/g, '').substring(0, 10);
    }

    function remove_els_errors(els) {
        for (var i = 0; i < els.length; ++i) {
            remove_error(els[i]);
        }
    }

    function is_email_or_phone_filled(email, phone, form) {
        if (!email.value && !phone.value) {
            var msg = 'Email or phone field is required!';
            insert_error_el(form, email, msg);
            insert_error_el(form, phone, msg);
            return false;
        } else {
            remove_els_errors([phone, email]);
        }
        return true;
    }

    function is_fields_filled(form) {
        var els = form.getElementsByClassName('form-required');
        var is_valid = true;
        for (var i = 0; i < els.length; ++i) {
            remove_error(els[i]);
            if (!els[i].value.length) {
                insert_error_el(form, els[i], 'This field is required!');
                is_valid = false;
            }
        }
        return is_valid;
    }

    function set_handlers_for_remove_errors() {
        var els = document.getElementsByTagName('input');
        for (var i = 0; i < els.length; ++i) {
            els[i].addEventListener('input', function(event) {
                remove_error(this);
                if (this.name == 'email' || this.name == 'phone') {
                    var phone = this.parentElement.parentElement.elements['phone'];
                    var email =this.parentElement.parentElement.elements['email'];
                    remove_els_errors([phone, email]);
                }
            }, false);
        }
    }

    set_handlers_for_remove_errors();

    document.getElementById('form-email').addEventListener('input', function(event) {
        var email = this;
        var form = this.parentElement.parentElement;
        is_email_valid(email, form);
    }, false);

    document.getElementById('post-form').addEventListener('submit', function(event) {
        event.preventDefault();
        var is_valid = true;

        var email = this.elements['email'];
        var password = this.elements['password'];
        var phone = this.elements['phone'];
        var form = this;

        is_valid *= is_fields_filled(form);
        if (this.className == 'news_item-form') {
            is_valid *= is_email_or_phone_filled(email, phone, form);
        }
        is_valid *= is_email_valid(email, form);
        is_valid *= is_password_valid(password, form);
        validate_phone(phone);

        if (is_valid) {
            this.submit();
        }
    }, false);
})()
