# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.db.models import F
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.db.models.aggregates import Count
#from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic import TemplateView, ListView, FormView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from datetime import date, datetime, timedelta
import re

from models import News, Comment
from .forms import RegistrationForm, CommentModelForm, LoginForm


class IndexTemplateView(TemplateView):
    template_name = u'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class NewsTemplateView(TemplateView):
    template_name = u'news.html'

    def get_context_data(self, **kwargs):
        context = super(NewsTemplateView, self).get_context_data(**kwargs)
        context = {
            'news_items': News.objects.all(),
            'user': self.request.user,
        }
        return context

# TODO: rewrite this
# https://docs.djangoproject.com/en/1.10/topics/class-based-views/mixins/#using-formmixin-with-detailview
# http://stackoverflow.com/questions/16931901/django-combine-detailview-and-formview


class NewsDetailView(DetailView):
    model = News
    template_name = u'news_item.html'
    form_class = CommentModelForm

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        form = self.form_class()
        self.object = self.get_object()
        comments = Comment.objects.filter(news=self.object)
        context.update({
            'form': form,
            'news': self.object,
            'comments': comments,
            'post_msg': False,
            'user': self.request.user,
        })
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        News.objects.filter(slug=self.object.slug).update(viewed=F('viewed')+1)
        self.object.refresh_from_db()
        context = self.get_context_data(**kwargs)
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.form_class
        self.object = self.get_object()
        form = form_class(self.object, self.request.user, self.request.POST)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        form.save()
        form = self.form_class()
        context = self.get_context_data(**kwargs)
        context.update({
            'post_msg': True,
        })
        return render(self.request, self.template_name, context=context)

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({
            'post_msg': False,
        })
        return render(self.request, self.template_name, context=context)


class NewsByListView(ListView):
    template_name = u'news.html'
    context_object_name = 'news_items'


class NewsByViewedListView(NewsByListView):
    queryset = News.objects.all().order_by('-viewed')


class NewsByCommentsListView(NewsByListView):
    queryset = sorted(
        News.objects.all(),
        key=lambda n: n.comment_set.count(),
        reverse=True
    )


class NewsByLatestListView(NewsByListView):
    queryset = News.objects.all().order_by('-created')


class RegistrationFormView(FormView):
    template_name = u'registration.html'
    success_url = '/'
    form_class = RegistrationForm

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        form.save()
        return super(RegistrationFormView, self).form_valid(form)


class LoginFormView(FormView):
    template_name = u'login.html'
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        user = authenticate(
            username=form.cleaned_data.get('name'),
            password=form.cleaned_data.get('password'),
        )
        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect(self.request.GET.get('next'))
        else:
            state = 'Пользователь и/или пароль неверные!'
        context = {
            'form': form,
            'state': state,
        }
        return render(self.request, self.template_name, context=context)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(request.GET.get('next'))


class PersonalListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    template_name = u'personal.html'
    context_object_name = 'news_list'

    def get_queryset(self):
        return News.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(PersonalListView, self).get_context_data(**kwargs)
        td = timezone.now() - self.request.user.date_joined
        time_from_registration = '%s дн., %s час., %s секунд.' % (
            td.days, td.seconds // 3600, (td.seconds // 60) % 60)
        news_count = News.objects.filter(user=self.request.user).\
            aggregate(count=Count('user'))['count']
        comment_count = Comment.objects.filter(user=self.request.user).\
            aggregate(count=Count('user'))['count']
        days = td.days if td.days != 0 else 1
        comment_count_day = float(comment_count) / days
        context.update({
            'time_from_registration': time_from_registration,
            'comment_count': comment_count,
            'news_count': news_count,
            'comment_count_day': comment_count_day,
            'next_for_logout': True,
        })
        return context
