# -*- coding: utf-8 -*-
from django.db import models
from tinymce.models import HTMLField

import re
from datetime import date


class News(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)
    slug = models.SlugField(verbose_name='slug')
    text = HTMLField(verbose_name='Текст')
    created = models.DateField(verbose_name='Дата создания')
    changed = models.DateField(verbose_name='Дата изменения')
    image = models.ImageField(verbose_name='Изображение', blank=True)
    viewed = models.IntegerField(
        verbose_name='Количество просмотров', blank=True, default=0)
    user = models.ForeignKey('auth.User', blank=True, null=True)
    editor = models.ForeignKey('editor.Editor', blank=True, null=True)

    @property
    def text_short(self):
        from django.utils.html import strip_tags
        count = 30
        stripped = strip_tags(self.text).strip()
        return ' '.join(re.split('\s', stripped)[:count+1]) + '...'

    @property
    def comment_count(self):
        return self.comment_set.count()

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('news_item', kwargs={'slug': self.slug})

    def __str__(self):
        return 'News: %s' % self.name


class User(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=255)
    lastname = models.CharField(
        verbose_name='Фамилия', max_length=255, null=True)
    registration_date = models.DateField(
        verbose_name='Дата регистрации', default=date.today)

    def __str__(self):
        return 'User: %s' % self.name


class Comment(models.Model):
    comment = models.TextField(verbose_name='Комментарий')
    date = models.DateField(
        verbose_name='Дата публикации', null=True, auto_now_add=True)
    user = models.ForeignKey('auth.User')
    news = models.ForeignKey('News')
    communication = models.CharField(
        verbose_name='Способ связи', max_length=255, null=True)

    def __str__(self):
        return 'Comment by %s: %s' % (self.user.username, self.comment[:25])
