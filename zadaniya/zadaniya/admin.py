from django.contrib import admin
from zadaniya.models import News, Comment


admin.site.register(News)
admin.site.register(Comment)
