# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User

import re
from .models import Comment


def password_check(password):
    regexp = '(?=.*\d+)(?=.*[a-z]+)(?=.*[A-Z])'
    return re.search(regexp, password) is not None and len(password) > 5


class RegistrationForm(forms.Form):
    name = forms.CharField(label=u'Имя пользователя', required=True)
    phone = forms.CharField(label=u'Телефон', required=False)
    email = forms.CharField(required=False)
    password = forms.CharField(label=u'Пароль', required=True)

    def clean_password(self):
        password = self.cleaned_data['password']
        is_valid = password_check(password)
        if not is_valid:
            msg = 'Password must contain digit, latin symbols (upper and lower), ' +\
                  'and it must contain at least 6 symbols'
            raise forms.ValidationError(msg)
        return password

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        re.sub('\D', '', phone)
        return phone[:10]

    def clean_email(self):
        email = self.cleaned_data['email']
        if len(email) != 0:
            if re.match('^[\w-]+@[\w-]+\.[\w-]+$', email) is None:
                msg = 'Email must be like "test@test.com"'
                raise forms.ValidationError(msg)
        return email

    def save(self):
        User.objects.create_user(
            username=self.cleaned_data.get('name'),
            email=self.cleaned_data.get('email'),
            password=self.cleaned_data.get('password'),
        )


class CommentModelForm(forms.ModelForm):
    def __init__(self, news=None, user=None, *args, **kwargs):
        if news is not None:
            self.news = news
            self.user = user
        super(CommentModelForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Comment
        fields = ('comment',)

    phone = forms.CharField(label=u'Телефон', required=False)
    email = forms.CharField(required=False)

    def save(self):
        comment = super(CommentModelForm, self).save(commit=False)
        comment.news = self.news
        comment.user = self.user
        comment.communication = self.cleaned_data['email'] or\
            self.cleaned_data['phone']
        comment.save()
        return comment

    def clean(self):
        cleaned_data = super(CommentModelForm, self).clean()
        msg = 'Email or phone field is required!'
        email_name = 'email'
        phone_name = 'phone'
        if not cleaned_data[email_name] and not cleaned_data[phone_name]:
            self.add_error(email_name, msg)
            self.add_error(phone_name, msg)
        return cleaned_data


class LoginForm(forms.Form):
    name = forms.CharField(label=u'Имя пользователя', required=True)
    password = forms.CharField(label=u'Пароль', required=True)
