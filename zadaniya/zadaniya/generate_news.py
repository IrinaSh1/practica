from zadaniya.models import News
import random
import datetime
import loremipsum

def generate_date():
    random.seed()
    seconds_in_two_weeks = 14 * 24 * 60 * 60
    differance = random.randrange(0, seconds_in_two_weeks)
    timedelta = datetime.timedelta(seconds = differance)
    random_date = datetime.datetime.now() - timedelta

    return random_date

def generate_news(count):
    for i in range(count):
        (n_sentences, n_words, news_text) = loremipsum.generate_paragraph()
        (n_sentences, n_words, news_name) = loremipsum.generate_sentence()
        news_slug = "_".join(news_name.split(" ")[0:3])

        News.objects.create(
            name = news_name[:200],
            content = news_text,
            slug = news_slug
        )
