class Comment(models.Model):
    content = models.TextField(null=True, blank=True)
    username = models.CharField(max_length=255)
    communication_type = models.CharField(max_length=20)
    news_item = models.ForeignKey('News')

    created = models.DateTimeField(auto_now_add=True)


def create_comment(request):
    form =  CommentModelForm(news_item=News.objects.get(id=request.GET.get('id')), request.POST)
    if form.is_valid():
        comment = form.save()



class CommentModelForm(forms.ModelForm):

    def __init__(self, news_item, *args, **kwargs):
        self.news_item = news_item
        super(CommentModelForm,self).__init__(*args, **kwargs)


    class Meta:
        model = Comment
        fields = ['content', u'username']


    email = forms.EmailField()
    phone = forms.CharField()


    def clean(self):
        cleaned_data = super(CommentModelForm, self).clean()
        raise ValidationError('Заполните почту или телефон!')

    def clean_phone(self):
        raise ValidationError()


    def save(self):
        comment = super(CommentModelForm, self).save(commit=False)
        comment.news_item = self.news_item
        comment.communication_type = self.cleaned_data.get(u'email') or self.cleaned_data.get(u'phone')
        comment.save()
        return comment


forms.errors[u'phone']
forms.errors[u'all']
