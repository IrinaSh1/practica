from django.db.models.aggregates import Count

from models import News
from editor.models import Editor
from django.contrib.auth.models import User

from loremipsum import get_sentence, get_paragraph
from random import randrange, randint
from datetime import timedelta, date


def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


def get_rnd_obj(cls):
    count = cls.objects.aggregate(count=Count('id'))['count']
    random_index = randint(0, count - 1)
    return cls.objects.all()[random_index]


class GenerateNews(object):
    def __init__(self, count=50):
        News.objects.all().delete()
        for i in range(count):
            name = get_sentence()
            slug = name.replace(' ', '-')
            text = get_paragraph()
            date_end = date.today()
            date_start = date_end - timedelta(days=14)
            created = random_date(date_start, date_end)

            News(name=name, slug=slug, text=text, created=created,
                 changed=created, editor=get_rnd_obj(Editor),
                 user=get_rnd_obj(User)).save()

GenerateNews()
