#*-coding:utf-8-*
import re

class Fruit(object):
    def __init__(self, name1, fruit1, quantity1, name2, fruit2, quantity2, name3, fruit3, quantity3):
        self.name1 = name1
        self.name2 = name2
        self.name3 = name3
        self.fruit1 = fruit1
        self.fruit2 = fruit2
        self.fruit3 = fruit3
        self.quantity1 = quantity1
        self.quantity2 = quantity2
        self.quantity3 = quantity3

    def get_full_str(self):
        return '{0} съел(а) {1}: {2}. {3} съел(а) {4}: {5}. {6} съел(а) {7}: {8}'\
        .format(self.name1, self.fruit1, self.quantity1, self.name2,
        self.fruit2, self.quantity2, self.name3, self.fruit3, self.quantity3)

    def __str__(self):
        return self.get_full_str()

    def quantity_apple(self):
        parrent = u"яблок\:\s(\d+)"
        return re.findall(parrent, self.get_full_str())



strings = Fruit("Аня", 'яблок', 5, "Вика", "яблок", 3, "Владислав", "дынь", 7)
print(strings.quantity_apple())
print(strings.get_full_str())
