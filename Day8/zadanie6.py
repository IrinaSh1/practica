import re

def zadanie6(email):
    pattern = '[a-z0-9@_-]'
    if re.search(pattern, email):
        return True
    else:
        return False

print (zadanie6('test@test.com'))
print (zadanie6('ira.sherstyukova.96@mail.ru'))
print (zadanie6('irina_shers-8322@rambler.ru'))
