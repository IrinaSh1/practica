import re

def zadanie4(password):
    lowercase = '[a-z]'
    capital = '[A-Z]'
    digit = '[0-9]'
    dog = '[@]'
    comma = '[,]'
    dot = '[.]'
    underscore = '[_]'
    dash = '[-]'
    if re.findall(lowercase, password)\
        and re.findall(capital, password)\
        and re.findall(digit, password)\
        and re.findall(dog, password)\
        and re.findall(comma, password)\
        and re.findall(dot, password)\
        and re.findall(underscore, password)\
        and re.findall(dash, password):
        return True
    else:
        return False

print(zadanie4('akfjl44757_6POJ9-3.8457@W,WGWE'))
